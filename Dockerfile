FROM ubuntu

# Install python
RUN apt-get update && apt-get install -y python3-dev python3 python3-pip

# Install jupyter
ADD requirements.txt .
RUN pip3 install -r requirements.txt 

# Install Nodejs
RUN apt-get update && apt-get install -y nodejs npm yarn

# Configure jupyter plugin for install extension
RUN jupyter contrib nbextension install --user
RUN jupyter nbextensions_configurator enable --user

# Configure sos (for multi lenguage into a notebook)
RUN python3 -m sos_notebook.install

# configure for mybinder
ARG NB_USER=jovyan
ARG NB_UID=1000
ENV USER ${NB_USER}
ENV NB_UID ${NB_UID}
ENV HOME /home/${NB_USER}

RUN adduser --disabled-password \
    --gecos "Default user" \
    --uid ${NB_UID} \
    ${NB_USER}

# Configure javascript
RUN npm install -g ijavascript
RUN ijsinstall

# Install tslab
ENV PATH $PATH:${HOME}/.npm-global/bin
RUN mkdir ~/.npm-global &&\
  npm config set prefix '~/.npm-global' &&\
  npm install -g tslab &&\
  tslab install

# WORKDIR
WORKDIR ${HOME}
COPY notebook ${HOME}/notebook

CMD jupyter notebook --ip=0.0.0.0 --port=8888 --allow-root --NotebookApp.token=''

